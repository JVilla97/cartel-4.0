package com.example.daniel.myweatherapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                getWeather(v);
                return true;
        }
        return false;
    }

    public void getWeather(View v) {

        EditText location = (EditText) findViewById(R.id.location);
        new GetWeatherBackground().execute(location.getText().toString());

    }

    private void errorBox() {

        AlertDialog.Builder errorBox = new AlertDialog.Builder(this);

        errorBox.setMessage("Location not found, try again boi");
        errorBox.setCancelable(false);
        errorBox.setNeutralButton("OK", null);
        errorBox.show();

    }

    private class GetWeatherBackground extends AsyncTask<String, Void, Weather> {
        HashMap<String, Bitmap> bitmaps = new HashMap<>();

        @Override
        protected Weather doInBackground(String... locations) {
            URL url;
            Bitmap bmp;
            // Get the text from the input field

            Weather w = new Weather(locations[0]);
            w.fetch();
            w.fetchRadar();

            try {
                url = new URL(w.getIcon());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                bitmaps.put("icon", bmp);

                for (int i = 0; i < 10; i++) {
                    url = new URL(w.getForecastIcon(i));
                    bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    bitmaps.put("" + i, bmp);
                }

                url = new URL(w.getRadarImage());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                bitmaps.put("radar", bmp);

            } catch (Exception e) {
                System.err.println("Got an Exception");
                e.printStackTrace();
            }

            return w;
        }

        @Override
        protected void onPostExecute(Weather w) {

            try {
                TextView temp = (TextView) findViewById(R.id.temp);
                temp.setText("" + w.getTemperatureF() + "°");

                TextView forecasthigh = (TextView) findViewById(R.id.forecasthigh);
                forecasthigh.setText("" + w.getHighF(0) + "°");

                TextView slash = (TextView) findViewById(R.id.slash);
                slash.setText("/");

                TextView forecastlow = (TextView) findViewById(R.id.forecastlow);
                forecastlow.setText("" + w.getLowF(0) + "°");

                TextView citystate = (TextView) findViewById(R.id.citystate);
                citystate.setText("" + w.getCityState());

                TextView conditions = (TextView) findViewById(R.id.conditions);
                conditions.setText("" + w.getWeather());

                TextView date = (TextView) findViewById(R.id.date);
                date.setText("" + w.getLongDate());

                ImageView current = (ImageView) findViewById(R.id.icon);
                Bitmap bmp = bitmaps.get("icon");
                current.setImageBitmap(bmp);
                current.setVisibility(View.VISIBLE);

                int[] textViewIdsDay = {R.id.day1, R.id.day2, R.id.day3, R.id.day4, R.id.day5, R.id.day6, R.id.day7, R.id.day8, R.id.day9, R.id.day10};
                int[] imageViews = {R.id.image1, R.id.image2, R.id.image3, R.id.image4, R.id.image5, R.id.image6, R.id.image7, R.id.image8, R.id.image9, R.id.image10};
                int[] textViewIdsHL = {R.id.highlow1, R.id.highlow2, R.id.highlow3, R.id.highlow4, R.id.highlow5, R.id.highlow6, R.id.highlow7, R.id.highlow8, R.id.highlow9, R.id.highlow10};
                TextView[] tvd = new TextView[10];
                TextView[] tvhl = new TextView[10];
                ImageView[] iv = new ImageView[10];

                for (int i = 0; i < 10; i++) {
                    tvd[i] = (TextView) findViewById(textViewIdsDay[i]);
                    tvd[i].setText("" + w.getDay(i));

                    iv[i] = (ImageView) findViewById(imageViews[i]);

                    bmp = bitmaps.get("" + i);
                    iv[i].setImageBitmap(bmp);
                    iv[i].setVisibility(View.VISIBLE);
                    tvhl[i] = findViewById(textViewIdsHL[i]);
                    tvhl[i].setText("" + w.getHighF(i) + "°" + " / " + w.getLowF(i) + "°");
                }

                ImageView radar = (ImageView) findViewById(R.id.radar);

                bmp = bitmaps.get("radar");
                radar.setImageBitmap(bmp);
                radar.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                e.printStackTrace();
                errorBox();
            }

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), imm.HIDE_NOT_ALWAYS);
        }
    }


}
